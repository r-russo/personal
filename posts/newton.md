--- 
title: Newton-Raphson
layout: layouts/post.njk
---

El método de Newton-Raphson sirve para hallar raíces a ecuaciones no lineales.
Es un método abierto y parte de un valor inicial a determinar. Su fórmula de
iteración es la siguiente:

<div class="equation">$$ x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)} $$</div>

Las condiciones suficientes para la convergencia del método son:

- $f'(x) \neq 0 \, \forall \, x \in [a,b]$: la derivada (denominador de la
  fórmula) debe ser distinta de cero en un intervalo de trabajo donde se
  encuentra el punto inicial y la raíz. En la función $f(x)$ esto se traduce
  como que no haya puntos de inflexión.
- $f''(x) > 0$ o $f''(x) < 0 \, \forall \, x \in [a,b]$: la derivada segunda
  debe mantener el signo dentro del intervalo de trabajo. En $f(x)$ se puede
  observar con que no haya cambio de concavidad.
- $\frac{|f(h)|}{|f'(h)|} \leq b - a$: el incremento introducido por algún
  punto, no debe exceder el intervalo.

## Ejemplo

*Calcular usando el método de la secante la primera intersección entre las
funciones $f(x) = \sin\left(\frac{x}{2}\right)$ y $g(x) = 5e^{-x}$ Graficar.*

Dado que este método puede aplicarse para hallar raíces de una función, hay que
adaptar el problema para poder aplicar el método. La intersección, no es nada
más ni nada menos que el valor de $x$ que hace que ambas funciones tengan el
mismo valor o, lo que es lo mismo, $f(\alpha) = g(\alpha)$. Dicho de otra
forma, la resta de las funciones debe ser cero por lo que podemos definir otra
función, a la que aplicaremos Newton-Raphson, como:

<div class="equation"> \begin{equation} F(x) \equiv f(x) - g(x) =
\sin\left(\frac{x}{2}\right) - 5 e^{-x} \label{eq:F} \end{equation} </div>

Primero debemos ubicar aproximadamente dónde se encuentra la raíz que queremos
hallar. En el siguiente gráfico se muestran el gráfico de la función $F(x)$ y
su derivada. Se puede observar que la raíz $\alpha \in [1,2]$. Derivando,
conocemos que su derivada es $F'(x) = \frac{1}{2} \cos\left(\frac{x}{2}
\right) + 5 e^{-x}$ y que se produce un cero en aproximadamente 3,5.

<figure id="grafico_f_df" ></figure>

Ahora podemos analizar las condiciones de convergencia para poder comenzar a
iterar y obtener la solución:

- $F'(x) \neq 0 \, \forall \, x \in [0, 3]$ según lo observado en el gráfico.
  Dado que la raíz se encuentra entre 1 y 2, podemos tomar un punto dentro de
  este intervalo.
- A partir del gráfico de la función , se observa que no hay cambios en la
  concavidad entre 1 y 2. Por lo tanto se espera que el método converja para un
  punto inicial perteneciente a este intervalo. De esta forma, tomamos como
  punto de partida $x_0 = 1$.

<div class="equation"> \begin{align*} x_1 &= x_0 - \frac{F(x_0)}{F'(x_0)} = 1 -
\frac{-1,3599}{2,2781} = 1,5970 \\ x_2 &= x_1 - \frac{F(x_1)}{F'(x_1)} = 1,8146
\\ x_3 &= x_2 - \frac{F(x_2)}{F'(x_2)} = 1,8384 \\ x_4 &= x_3 -
\frac{F(x_3)}{F'(x_3)} = 1.8386 \\ x_5 &= x_4 - \frac{F(x_4)}{F'(x_4)} = 1.8386
\\ \end{align*} </div>

Visualmente, el resultado es el siguiente:

<figure id="grafico_anim"></figure> 

Viendo que para cuatro cifras decimales el método se estanca, detenemos en la
quinta iteración y analizamos errores absolutos para $F(x_i)$:

<div class="equation"> \begin{align*} \epsilon_1 &= |F(x_1)| = 0,2963 \\
\epsilon_2 &= |F(x_2)| = 0,0267 \\ \epsilon_3 &= |F(x_3)| = 0,0003 \\
\epsilon_4 &= |F(x_4)| = 0,0000 \\ \epsilon_5 &= |F(x_5)| = 0,0000 \\
\end{align*} </div>

Por último, se halla el punto de intersección entre ambas funciones y, por lo
analizado anteriormente, se espera que sean iguales:

<div class="equation"> \begin{equation*} f(x_5) = g(x_5) = 0,7951
\end{equation*} </div>

<script src="/js/newton.js"></script>
