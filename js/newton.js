// F, dF
let grafico_f_df = document.getElementById("grafico_f_df");

const f = (x) => Math.sin(x / 2) - 5 * Math.exp(-x);
const df = (x) => (1 / 2) * Math.cos(x / 2) + 5 * Math.exp(-x);
const step = 0.2;

let x = [];

for (let i = 0; i < 5; i = i + step) {
    x.push(i);
}

let trace1 = {
    x: x,
    y: x.map(f),
    mode: "lines",
    name: "F(x)",
};

let trace2 = {
    x: x,
    y: x.map(df),
    mode: "lines",
    name: "F'(x)",
};

const trace = [trace1, trace2];

Plotly.newPlot(grafico_f_df, trace, {});

// Animación
const grafico_anim = document.getElementById("grafico_anim");
const trace_f = trace1;

const data = [
    {
        x: x,
        y: x.map(f),
    },
];

const layout = {
    sliders: [
        {
            pad: { t: 30 },
            x: 0.05,
            len: 0.95,
            currentvalue: {
                xanchor: "right",
                prefix: "F(x) = ",
                font: {
                    color: "#888",
                    size: 20,
                },
            },
            transition: { duration: 500 },
            steps: [
                {
                    label: "0",
                    method: "animate",
                    args: [
                        ["0"],
                        {
                            mode: "immediate",
                            frame: { redraw: false, duration: 500 },
                            transition: { duration: 500 },
                        },
                    ],
                },
                {
                    label: "1",
                    method: "animate",
                    args: [
                        ["1"],
                        {
                            mode: "immediate",
                            frame: { redraw: false, duration: 500 },
                            transition: { duration: 500 },
                        },
                    ],
                },
                {
                    label: "2",
                    method: "animate",
                    args: [
                        ["2"],
                        {
                            mode: "immediate",
                            frame: { redraw: false, duration: 500 },
                            transition: { duration: 500 },
                        },
                    ],
                },
            ],
        },
    ],
};

const frames = [
    {
        name: "0",
        data: [
            {
                y: [2, 1, 3],
                "line.color": "red",
            },
        ],
    },
    {
        name: "1",
        data: [
            {
                y: [3, 2, 1],
                "line.color": "green",
            },
        ],
    },
    {
        name: "2",
        data: [
            {
                y: [1, 3, 2],
                "line.color": "blue",
            },
        ],
    },
];

Plotly.newPlot(grafico_anim, {
    data: data,
    layout: layout,
});
